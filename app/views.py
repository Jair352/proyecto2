
from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    return render(request, 'app/index.html')


def registro(request):
    return render(request, 'app/registro/registro.html')


############################################################################################################################################
#########################** FUNCIONES ADMIN ***##################################################################################################
############################################################################################################################################
# Lista de Grupos de Admin
lista_grupos_admin = [
    {'id': 1, 'codigogrupo': 101837, 'asignatura': 'Cocina', 'semestre': 3, 'profesor': 'Alex Ferrero',
     'numeroestudiante': 3, },
    {'id': 2, 'codigogrupo': 203244, 'asignatura': 'Matematicas', 'semestre': 5, 'profesor': 'Andres',
     'numeroestudiante': 6, },
    {'id': 3, 'codigogrupo': 67907, 'asignatura': 'Fisica', 'semestre': 3, 'profesor': 'Juliana',
     'numeroestudiante': 30, },
    {'id': 4, 'codigogrupo': 95894, 'asignatura': 'Calculo', 'semestre': 5, 'profesor': 'Juliana',
     'numeroestudiante': 40, },
]
lista_profesores_admin = [
    {'apellido': 'Acosta Mendez', 'nombres': 'Alexandra',
        'email': 'alexandrac@gmail.com', 'idgrupos': 1, },
    {'apellido': 'Cardenas Hernandez', 'nombres': 'Camilo',
        'email': 'cardebascage@hotmail.com', 'idgrupos': 1, },
]

lista_estudiantes_admin = [
    {'id': 1, 'codigo': 23235, 'apellido': 'Acosta Mendez', 'nombres': 'Alexandra',
        'email': 'alexandrac@gmail.com', 'idgrupos': 1, },
    {'id': 2, 'codigo': 87558, 'apellido': 'Cardenas Hernandez', 'nombres': 'Camilo',
     'email': 'cardenascahe@hotmail.com', 'idgrupos': 2, },
    {'id': 3, 'codigo': 87589, 'apellido': 'Esquivel Rodrigues', 'nombres': 'Fernando',
     'email': 'fer@gmail.com', 'idgrupos': 3, },
]

lista_grupos_profesor = [
    {'idgrupos': 1, 'semestres': 3, 'codigodegrupo': 1213, 'asignatura': 'Mercadeo'},
    {'idgrupos': 2, 'semestres': 6, 'codigodegrupo': 3456, 'asignatura': 'proyecto'},
    {'idgrupos': 3, 'semestres': 9, 'codigodegrupo': 7896, 'asignatura': 'musica'},
]

lista_actividades_profesor = [
    {'id' : 1, 'descripcion': 'Tarea', 'codigodelgrupo': 214135, 'asignaturadelgrupo': 'Mercadeo',
     'semestredelgrupo': 6},
    {'id' : 2, 'descripcion': 'parcial', 'codigodelgrupo': 78644, 'asignaturadelgrupo': 'Fisica',
     'semestredelgrupo': 3},
    {'id' : 3, 'descripcion': 'taller', 'codigodelgrupo': 878094, 'asignaturadelgrupo': 'Calculo', 
     'semestredelgrupo': 9},
]

lista_estudiantes_profesor = [
    {'idgrupo': 1, 'idestudiante': 2232377, 'apellidos': 'Cortazar Guillen', 'nombres': 'Alejandro',
     'email': 'guillencortazaralejandro@gmail.com', },
    {'idgrupo': 2, 'idestudiante': 989327, 'apellidos': 'Duran Duarte', 'nombres': 'Felipe',
     'email': 'fe@gmail.com', },
    {'idgrupo': 3, 'idestudiante': 329083, 'apellidos': 'Hernandez Tinoco', 'nombres': 'Harnold',
     'email': 'hernandez@gmail.com', },
]


def inicioadmin(request):
    return render(request, 'app/admin/admin.html')


def adminagregarstudiante(request, id):
     # print(lista_grupos_admin)
     for lista in lista_grupos_admin:
          if lista['id'] == id:
               contextoagregarestudiante = {
                    'grupo': lista
               }
               return render(request, 'app/admin/adminagregarstudiante.html', contextoagregarestudiante)

     return render(request, 'app/admin/adminagregarstudiante.html')


def admincreare(request):
    return render(request, 'app/admin/admincreare.html')


def admincrearg(request):
    return render(request, 'app/admin/admincrearg.html')


def admincrearprofesor(request):
    return render(request, 'app/admin/admincrearprofesor.html')


def admineditare(request, id):
     # print(lista_grupos_admin)
     for lista in lista_estudiantes_admin:
          if lista['id'] == id:
               contextoeditarestudiante = {
                    'estudiante': lista
               }
               return render(request, 'app/admin/admineditare.html', contextoeditarestudiante)

     return render(request, 'app/admin/admineditare.html')


def admineditarg(request, id):

    for lista in lista_grupos_admin:
        if lista['id'] == id:
            contexto = {
                'grupo': lista
            }
            return render(request, 'app/admin/admineditarg.html', contexto)

    return render(request, 'app/admin/admineditarg.html')


def admineliminarg(request, id):
     # print(lista_grupos_admin)
     for lista in lista_grupos_admin:
          if lista['id'] == id:
               contextoeliminargrupo = {
                    'grupo': lista
               }
               return render(request, 'app/admin/admineliminarg.html', contextoeliminargrupo)
     return render(request, 'app/admin/admineliminarg.html')


def adminevlea(request):
    contextoe = {
        'listae': lista_estudiantes_admin
    }
    return render(request, 'app/admin/adminevlea.html', contextoe)


def admingvgga(request, id):
     # print(lista_grupos_admin)
     for lista in lista_grupos_admin:
          if lista['id'] == id:
               contextoinfag = {
                    'grupo': lista
               }
               return render(request, 'app/admin/admingvgga.html', contextoinfag)
     return render(request, 'app/admin/admingvgga.html',)


def admingvlg(request):
    # contexto
    contexto = {
        'listag': lista_grupos_admin
    }
    return render(request, 'app/admin/admingvlg.html', contexto)


def adminpvlp(request):
    contextop = {
        'listap': lista_profesores_admin
    }
    return render(request, 'app/admin/adminpvlp.html', contextop)


def adminpvlpa(request):
    return render(request, 'app/admin/adminpvlpa.html')

    # return HttpResponse('aaaaaaaaaaaa')


def profesoractividadp(request):
    contextopa = {
        'listaprofesora': lista_actividades_profesor
    }
    return render(request, 'app/profesor/profesoractividadp.html', contextopa)

############################################################################################################################################
#########################** FUNCIONES PROFESOR ***##################################################################################################
############################################################################################################################################


def inicioprof(request):
    return render(request, 'app/profesor/profesor.html')


def profesoravaa(request):
    return render(request, 'app/profesor/profesoravaa.html')


def profesorcalificaractividad(request):
    #print(request, id)
#    for lista in lista_actividades_profesor:
#        print(lista_actividades_profesor)
#        if lista['id'] == id:
#            contextocalificar = {
#                'act': lista
#            }
#            return render(request, 'app/profesor/profesorcalificaractividad.html', contextocalificar)
#        
    return render(request, 'app/profesor/profesorcalificaractividad.html')


def profesorcrearcaractidad(request):
    return render(request, 'app/profesor/profesorcrearactividad.html')


def profesoreditaractividad(request):
    return render(request, 'app/profesor/profesoreditaractividad.html')


def profesorgveg(request):
    return render(request, 'app/profesor/profesorgveg.html')


def profesorgvggp(request):
    return render(request, 'app/profesor/profesorgvggp.html')


def profesorgvlg(request):
    contextopg = {
        'listapg': lista_grupos_profesor
    }
    return render(request, 'app/profesor/profesorgvlg.html', contextopg)


def profesorprincipale(request):
    contextopes = {
        'listaprofesore': lista_estudiantes_profesor
    }
    return render(request, 'app/profesor/profesorprincipale.html', contextopes)
###############################
# ###########Estudiantes######


def inicioestudiante(request):
    return render(request, 'app/estudiante/estudiantes.html')


def estudiantevlge(request):
    return render(request, 'app/estudiante/estudiantevlge.html')

    


def estudiantemateria(request):
    return render(request, 'app/estudiante/estudiantemateria.html')