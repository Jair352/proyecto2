from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    #Pantalla Principal de la aplicacion
    path('index/', views.index, name='index'),
    #pantallas principales de login
    path('index/registro/', views.registro, name='registro'),
    path('index/profesor/', views.inicioprof, name='profesor'),
    path('index/admin/', views.inicioadmin, name='admin'),
    path('index/estudiante/', views.inicioestudiante, name='Estudiante'),


    # urls admin  ################################################################################################################################
    # Pantalla de Grupo Principal
    path('index/admin/grupos/', views.admingvlg, name='Grupos'),
    # Cuando Selecciona Una Id De Un Grupo Especifico Ejemplo""Cocina"
    path('index/admin/grupos/<int:id>/', views.admingvgga, name='infga'),
    #Agregar Estudiante a Grupos...ER
    path('index/admin/grupos/AE/<int:id>/', views.adminagregarstudiante, name='AgregarEstudiante'),
    #Crear Grupos
    path('index/admin/grupos/crearg', views.admincrearg, name='Crear Grupo'),
    #Editar Grupo
    path('index/admin/grupos/editarg/<int:id>/', views.admineditarg, name='Editar Grupo'),
    #Eliminar Grupo
    path('index/admin/grupos/eliminarg/<int:id>/', views.admineliminarg, name='EliminarGrupo'),
    
    
    #Admin Profesores
    #Pantalla de Profesores Principal
    path('index/admin/profesores/', views.adminpvlp, name='Profesores'),
    #Cuando Selecciona Una Id De un Profesor Especifico Ejemplo "Alexandra Acosta Mendez"
    path('index/admin/profesores/Alexandra', views.adminpvlpa, name='Alexandra'),
    #Crear Profesor
    path('index/admin/profesores/crearp', views.admincrearprofesor, name='Crear Profesor'),
    
    
    #Admin Estudiantes
    #PAntalla De Estudiantes Principal
    path('index/admin/estudiantes/', views.adminevlea, name='Estudiantes'),
    #Crear Estudiante
    path('index/admin/estudiantes/creare', views.admincreare, name='Crear Estudiante'),
    #Editar Estudiante
    path('index/admin/estudiantes/editare<int:id>/', views.admineditare, name='EditarEstudiante'),
    
    # urls profesor ################################################################################################################################
    #Pantalla Principal de Actividades
    path('index/profesor/actividades/', views.profesoractividadp, name='Actividades'),
    #Ver ACtividad especifica en cierto grupo 
    path('index/profesor/actividades/acvidadg', views.profesoravaa, name='Actividades Grupo'),
    #Calificar la actividad
    path('index/profesor/actividades/calificarAc/', views.profesorcalificaractividad, name='calificaractividad'),
    #Crear Actividad
    path('index/profesor/actividades/creara', views.profesorcrearcaractidad, name='Crear Actividad'),
    #Editar Actividad
    path('index/profesor/actividades/editara', views.profesoreditaractividad, name='Editar Actividad'),
    
    #Principal de estudiantes
    path('index/profesor/estudiantes/', views.profesorprincipale, name='Principal Estudiantes'),
    #Cuando seleccione ver un grupo especifico estudiantes de un determinaod grupo "ejemplo 2232377"
    path('index/profesor/estudiantes/2232377', views.profesorgveg, name='2232377'),
    
    #Principal de Grupos
    #Lista Grupos
    path('index/profesor/grupos/', views.profesorgvlg, name='Grupos1'),
    #Lista de grupo especifico por seleccion de id
    path('index/profesor/grupos/1', views.profesorgvggp, name='Grupo 1'),

    #Estudiantes
    #Grupo Principal del estudiante
    path('index/estudiante/grupos', views.estudiantevlge, name='Grupos Estudiante'),
    #estudiante mirando materia 
    path('index/estudiante/grupos/materia', views.estudiantemateria, name='estudiantemateria'),
    
    ]

